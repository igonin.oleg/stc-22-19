package newVersion;

//Появилась информация об импортах в данном классе (об ресурсах, которые взяты из сторонних библиотек (зависимостей))
import lombok.*;
//Аннотация - это метка, которая задает классу определенную функциональность
@AllArgsConstructor //аннотация, которая говорит компилятору, что при создании объекта данного класса, должны участвовать все поля
@NoArgsConstructor //аннотация, которая говорит компилятору, что у этого класса есть пустой конструктор
@Builder
@Getter //аннотация, которая говорит компилятору, что у всех полей данного класса должны быть геттеры
@Setter //аннотация, которая говорит компилятору, что у всех полей данного класса должны быть сеттеры
public class Car {

    private String brand;
    private String model;
    private String color;
//    public Car() {}

//    public Car(String brand, String model, String color) {
//        this.brand = brand;
//        this.model = model;
//        this.color = color;
//    }
//
//    public String getBrand() {
//        return brand;
//    }
//
//    public void setBrand(String brand) {
//        this.brand = brand;
//    }
//
//    public String getModel() {
//        return model;
//    }
//
//    public void setModel(String model) {
//        this.model = model;
//    }
//
//    public String getColor() {
//        return color;
//    }
//
//    public void setColor(String color) {
//        this.color = color;
//    }
}
