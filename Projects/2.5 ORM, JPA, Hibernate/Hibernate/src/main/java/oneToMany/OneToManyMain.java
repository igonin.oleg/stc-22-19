package oneToMany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class OneToManyMain {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure();

        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
             Session session = sessionFactory.openSession()) {
            Airplane airplane = Airplane.builder()
                    .name("Boing 777")
                    .build();

            Passenger passenger1 = Passenger.builder()
                    .firstName("Oleg")
                    .lastName("Igonin")
                    .airplane(airplane)
                    .build();
            Passenger passenger2 = Passenger.builder()
                    .firstName("Vasya")
                    .lastName("Pupkin")
                    .airplane(airplane)
                    .build();
            Passenger passenger3 = Passenger.builder()
                    .firstName("Iezekel")
                    .lastName("Vasgenovich")
                    .airplane(airplane)
                    .build();

            session.save(airplane);
            session.save(passenger1);
            session.save(passenger2);
            session.save(passenger3);
        }
    }
}
