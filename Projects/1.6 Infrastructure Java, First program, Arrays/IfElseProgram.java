class IfElseProgram {
	public static void main(String[] args) {
		int a = 10;

		if (a > 10) {
			System.out.println("Number a > 10");
		} else if (a == 10) {
			System.out.println("Number a = 10");
		} else {
			System.out.println("Number a < 10");
		}
	}
}