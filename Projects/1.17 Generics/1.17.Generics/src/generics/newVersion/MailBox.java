package generics.newVersion;
//<КАКАЯ-ЛИБО_БУКВА> - diamonds, конструкция класса (или интерфейса) дженерика
public class MailBox<T> {

    private T distribution;

    public MailBox(T distribution) {
        this.distribution = distribution;
    }

    public T getDistribution() {
        return distribution;
    }

    public void setDistribution(T distribution) {
        this.distribution = distribution;
    }
}
