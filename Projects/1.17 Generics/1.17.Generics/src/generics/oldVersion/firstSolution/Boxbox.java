package generics.oldVersion.firstSolution;

public class Boxbox {

    private Box box;

    public Boxbox(Box box) {
        this.box = box;
    }

    public Box getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box;
    }
}
