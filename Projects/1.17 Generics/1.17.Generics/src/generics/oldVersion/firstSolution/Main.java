package generics.oldVersion.firstSolution;

public class Main {
    public static void main(String[] args) {

//        Mail mail = new Mail("Привет из города Казани от Олега");
//        MailBox mailBox = new MailBox(mail);
//        //1. Отрабатывает mailBox.getMail() - на место данного выражения
//        //подставляется объект типа Mail
//        //2. ОБЪЕКТА_ТИПА_MAIL.getMessage() - на место данного выражения
//        //подставляется строка, которую возвращает данный метод
//        System.out.println(mailBox.getMail().getMessage());
//
//        mailBox.setMail(new Mail("Прощальное слово из города Казани"));
//        System.out.println(mailBox.getMail().getMessage());

//        Magazine magazine = new Magazine("Мурзилка");
//        MagazineBox magazineBox = new MagazineBox(magazine);
//
//        System.out.println(magazineBox.getMagazine().getName());

        Box box = new Box("Мотоцикл для Олега");
        Boxbox boxbox = new Boxbox(box);

        System.out.println(boxbox.getBox().getName());
    }
}
