package gibdd;

import java.util.*;

public class GIBDD {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Добро пожаловать в ГИБДД");
        System.out.println("Введите номер, который хотите зарегистрировать");
        Set<Number> numberSet = new HashSet<>();


        while (true) {
            System.out.println(Arrays.toString(numberSet.toArray()));

            String number = scanner.nextLine();

            if (number.equals("stop")) {
                return;
            }

            if (numberSet.add(new Number(number))) {
                System.out.println("Ваш номер успешно зарегистрирован!");
            } else {
                System.out.println("Данный номер уже существует");
            }
        }
    }
}
