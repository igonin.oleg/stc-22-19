package ru.stc.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootLocalizationValidationApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootLocalizationValidationApplication.class, args);
    }

}
