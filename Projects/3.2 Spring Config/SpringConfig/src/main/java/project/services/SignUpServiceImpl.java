package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import project.models.Account;
import project.validators.PasswordBlackList;
import project.validators.PasswordValidator;
import project.validators.EmailValidator;

//@Component, @Service, @Repository
//TODO: разница между @Component, @Service, @Repository
@Service
public class SignUpServiceImpl implements SignUpService {


    private final EmailValidator emailValidator;
    private final PasswordValidator passwordValidator;
    private final PasswordBlackList passwordBlackList;

    //Аннотация @Autowired - (правило хорошего тона) ставится на конструкторы и сеттеры
    //Мы указываем спрингу, что сюда нужно заижектить (вставить) бины
    //Производит обязательный инжект в конструктор или сеттер
    @Autowired
    public SignUpServiceImpl(@Qualifier("emailValidatorRegex") EmailValidator emailValidator,
                             @Qualifier("passwordValidatorCharacter") PasswordValidator passwordValidator,
                             PasswordBlackList passwordBlackList) {
        this.emailValidator = emailValidator;
        this.passwordValidator = passwordValidator;
        this.passwordBlackList = passwordBlackList;
    }

    @Override
    public void signUp(String email, String password) {

        if (passwordBlackList.contains(password)) {
            throw new IllegalArgumentException("Ваш пароль уже был взломан");
        }

        emailValidator.validate(email);
        passwordValidator.validate(password);


        Account account = Account.builder()
                .email(email)
                .password(password)
                .build();


        System.out.println("Аккаунт создан");
        System.out.println("Email - " + account.getEmail());
        System.out.println("Password - " + account.getPassword());
    }
}
