package project.validators;

public interface PasswordValidator {

    void validate(String password);
}
