import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NumberStreamExample {
    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < 50; i++) {
            integers.add(random.nextInt(100));
        }

        System.out.println(Arrays.toString(new List[]{integers}));
        System.out.println("Старая версия:");
        for (int i = 0; i < integers.size(); i++) {
            if (integers.get(i) % 2 == 0) {
                System.out.print(integers.get(i) + " ");
            }
        }
        System.out.println();
        System.out.println("*************************");
        System.out.println("Новая версия, через стримы:");
//        Stream<Integer> streamIntegers = integers.stream();
//        streamIntegers.filter(number -> number %2 == 0)
//                .sorted()
//                .distinct()
//                .forEach(stroke -> System.out.print(stroke + " "));
        integers.stream()
                .filter(number -> number % 2 == 0)
                .sorted()
                .distinct()
                .forEach(stroke -> System.out.print(stroke + " "));

        List<String> strokesIntegers = integers.stream()
                .map(number -> {
                    String stroke = String.valueOf(number);
                    return stroke;
                }).collect(Collectors.toList());

        System.out.println();

        //::(квадроточие) - ссылка на метод
        strokesIntegers.stream()
                .map(Integer::parseInt)
                .distinct()
                .sorted()
                .forEach(number -> System.out.print(number + " "));
    }
}
