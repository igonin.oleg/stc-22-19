package newVersion.lyambda;
//Тестовый интерфейс, метод которого принимает на вход два аргумента, но ничего не возвращает
public interface TwoArgumentsInterface {

    void multiplication(int numberOne, int numberTwo);
}
