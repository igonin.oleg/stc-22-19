package oldVersion;

//Имплементация интерфейса UtilNumbers
//Метод checkNumbers проверяет на нечетность (в консоль будут выводиться нечетные числа)
public class OddNumbers implements UtilNumbers {
    @Override
    public void checkNumbers(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 != 0) {
                System.out.println(numbers[i] + " - нечетное число");
            }
        }
    }
}
