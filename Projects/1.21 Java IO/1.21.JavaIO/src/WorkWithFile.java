import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class WorkWithFile {

    public static void main(String[] args) {
        File file = new File("Names.txt");

        try (Writer writer = new FileWriter(file, true)) {
            file.createNewFile();
            writer.write("Irina\n");
            writer.write("Alexander\n");
            writer.write("Andrei\n");
            writer.write("Anton\n");
            writer.write("Artem\n");
            writer.write("Denis\n");
        } catch (IOException e) {
            throw new RuntimeException();
        }

    }
}
