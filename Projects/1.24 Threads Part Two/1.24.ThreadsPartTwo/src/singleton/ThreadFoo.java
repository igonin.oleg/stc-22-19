package singleton;

public class ThreadFoo extends Thread {

    @Override
    public void run() {
        Singleton singleton = Singleton.getInstance("FOO");
        System.out.println(singleton.getMessage());
    }
}
