package singleton;

public class Main {

    public static void main(String[] args) {
//        Singleton singleton1 = Singleton.getInstance("FOO");
//        Singleton singleton2 = Singleton.getInstance("BAR");
//
//        System.out.println(singleton1.getMessage());
//        System.out.println(singleton2.getMessage());

        ThreadBar threadBar = new ThreadBar();
        ThreadFoo threadFoo = new ThreadFoo();

        threadFoo.start();
        threadBar.start();
    }
}
