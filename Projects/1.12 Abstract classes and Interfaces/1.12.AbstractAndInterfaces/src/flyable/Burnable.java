package flyable;

public interface Burnable {

    void burn();
}
