package flyable;

public class Airplane implements Flyable, Burnable {
    @Override
    public void fly() {
        System.out.println("Я - самолет, я лечу");
    }

    @Override
    public void burn() {
        System.out.println("Самолет ночью загорелся. Никто не пострадал");
    }
}
