package flyable;

public interface Flyable {

    void fly();
}
