package inheritance;

public class Main {
    public static void main(String[] args) {
        Car car = new Car("Cherry", "Amulet");
        SportCar sportCar = new SportCar("Audi", "RS6");
        OffRoadCar offRoadCar = new OffRoadCar("Lada", "Niva");

        car.go();
        sportCar.go();
        offRoadCar.go();
    }
}
