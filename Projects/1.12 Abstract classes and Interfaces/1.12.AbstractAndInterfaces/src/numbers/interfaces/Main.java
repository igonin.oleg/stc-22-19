package numbers.interfaces;

public class Main {
    public static void main(String[] args) {
        Condition condition = new ConditionEvenImpl();
        //Condition condition = new ConditionThreeImpl();
        //Condition condition = new ConditionFiveImpl();

        for (int i = 0; i < 5; i++) {
            System.out.println(i + " - " + condition.isOk(i));
        }
    }
}
