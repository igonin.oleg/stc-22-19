package numbers.interfaces;

public class ConditionThreeImpl implements Condition{
    @Override
    public boolean isOk(int number) {
        return number % 3 == 0;
    }
}
