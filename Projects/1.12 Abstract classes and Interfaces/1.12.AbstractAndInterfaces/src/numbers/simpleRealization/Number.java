package numbers.simpleRealization;

public class Number {

    public boolean isEven(int number) {
        if (number == 0) {
            return false;
        }
        boolean result = number % 2 == 0;

        return result;
    }

    public boolean isThree(int number) {
        if (number == 0) {
            return false;
        }

        boolean result = number % 3 == 0;

        return result;
    }

    public boolean isFive(int number) {
        if (number == 0) {
            return false;
        }

        boolean result = number % 5 == 0;

        return result;
    }
}
