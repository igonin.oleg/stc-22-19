package autofactory.poly;

public class OffRoadCar extends Car {

    private boolean isAllWheels;

    public OffRoadCar(String brand, String model, boolean isAllWheels) {
        super(brand, model);
        this.isAllWheels = isAllWheels;
    }

    public void stuck() {
        System.out.println("Мы застряли. Я за трактором");
    }
    @Override
    public void drive() {
        System.out.println("Мы на джипе. Мы едем прямо.");
    }

    public boolean isAllWheels() {
        return isAllWheels;
    }

    public void setAllWheels(boolean allWheels) {
        isAllWheels = allWheels;
    }
}
