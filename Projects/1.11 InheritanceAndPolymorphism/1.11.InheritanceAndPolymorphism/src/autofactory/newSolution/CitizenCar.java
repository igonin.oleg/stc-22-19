package autofactory.newSolution;

public class CitizenCar extends Car {

    private boolean isSafe;

    //super() - ключевое слово super() используется для вызова конструктора класса предка.
    //конструктор предка всегда должен вызываться в самом начале конструктора потомка.
    public CitizenCar(String brand, String model, boolean isSafe) {
        super(brand, model);
        this.isSafe = isSafe;
    }

    @Override
    public void drive() {
        System.out.println("Мы едем на гражданском авто. Мы в безопасности");
    }

    public boolean isSafe() {
        return isSafe;
    }

    public void setSafe(boolean safe) {
        isSafe = safe;
    }
}
