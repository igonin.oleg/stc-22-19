package autofactory.notInheritance;

public class AnotherClass extends Monkey {
    public AnotherClass(String name, String nickname) {
        super(name, nickname);
    }
}
