package autofactory.notInheritance;

public class Main {

    public static void main(String[] args) {
        Animal animal = new Animal("Животное");
        Monkey monkey = new Monkey("Обезьяна", "ЧиЧи");
        Human human = new Human("Vasiliy", "Topor", "Penkov");

//        System.out.println(animal.getName());
//        System.out.println(monkey.getNickname());
//        System.out.println(human.getName());
//        System.out.println(human.getLastName());
//        System.out.println(human.getNickname());

        human.voice();
    }
}
