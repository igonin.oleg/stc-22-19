package autofactory.notInheritance;

public class Animal {

    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public void voice() {
        System.out.println("AUUUUUUUUU");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
