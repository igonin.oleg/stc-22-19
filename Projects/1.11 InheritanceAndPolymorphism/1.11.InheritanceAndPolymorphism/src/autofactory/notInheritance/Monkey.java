package autofactory.notInheritance;

public class Monkey extends Animal {

    private String nickname;

    public Monkey(String name, String nickname) {
        super(name);
        this.nickname = nickname;
    }

    @Override
    public void voice() {
        System.out.println("U u ua ua ua");
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
