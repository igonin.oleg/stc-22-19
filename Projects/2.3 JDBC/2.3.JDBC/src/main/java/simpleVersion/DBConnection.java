package simpleVersion;


import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DBConnection {

    public Statement createStatement() throws IOException, SQLException, ClassNotFoundException {
        //Класс, в объект которого мы будем загружать настройки из файла db.properties
        Properties properties = new Properties();

        //Считываем из файла db.properties настройки
        FileInputStream fileInputStream = new FileInputStream("src/main/resources/db.properties");
        //Кладем настройки в properties
        properties.load(fileInputStream);
        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection(
                properties.getProperty("db.url"),
                properties.getProperty("db.user"),
                properties.getProperty("db.password")
        );
        //Возвращаем statement, через который мы будем отправлять запросы в БД
        Statement statement = connection.createStatement();

        return statement;
    }
}
