package waitAndNotify;

public class MyThread extends Thread {

    @Override
    public void run() {
        while (true) {
            System.out.println("Мы в нашем потоке");
            try {
                sleep(100);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
