package creatingThreads.runImplThread;

public class RunnableImpl implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("Поведение потока взято из объекта класса,");
            System.out.println("который реализует интерфейс Runnable");
            System.out.println("*****************************");
        }
    }
}
