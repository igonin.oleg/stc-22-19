package creatingThreads.runThread;
//2. способ - передать в конструктор класса Thread лябмда-выражение,
//в котором будет описано поведение данного потока
public class LambdaThread {

    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("Поведение потока взято из лямбды");
            }
        });

        thread.start();
    }
}
