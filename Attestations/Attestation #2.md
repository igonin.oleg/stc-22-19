## Аттестация №2

Нужно написать контроллер, который будет отрабатывать на запрос "/hello-page". Результатом отправки, в браузере, запроса на url "/hello-page" должна открыться страница с приветствием пользователя.  

Вместо написания котроллера, указанного выше, можно предоставить проектную работу (для сдачи данной аттестации конечная готовность проекта не имеет значение), в которой есть минимум один контроллер.